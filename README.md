# README #

### What is this repository for? ###

* Ejemplo de aplicación fuse que permite leer un fichero en otra ubicación
* Prestar atención a cómo se toma el path absoluto del fichero con la función realpath(), para poder abrirlo después en open sin problemas
* Los metadatos de los directorios y ficheros en FUSE se toman del fichero original (tamaño, fechas, propietario, etc.)
* Hay que implementar la funciones de abrir (open) , leer (read) y cerrar (release) el fichero en FUSE
* La funcion open hay que hacerla con el path completo y los flags que nos pasan en FUSE
* La funcion release se traduce a una llamada a close usando el file_handler que guardamos en el open
* La lectura se traduce a una lectura con pread() del fichero original, usando el file_handler que guardamos en el open

### How do I get set up? ###

* Usar la herramienta make
* hay reglas para compilar, montar, depurar, etc. (ejemplo_fuse, mount, umount, debug, test)

### Who do I talk to? ###

* Andrés Rodríguez <andres at uma dot es>
