/*
  FUSE: Filesystem in Userspace
*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>


struct structura_mis_datos
{
    char * path;							/* path completo del fichero inicial */
    char * fichero_inicial;  				/* fichero inicial*/
    struct timespec st_atim;  				/* fechas del fichero */
    struct timespec st_mtim;
    struct timespec st_ctim;
    unsigned long st_size;
    uid_t     st_uid;        				/* El usuario y grupo */
    gid_t     st_gid;
};

static int ejemplo_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;

    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {  // dir raiz
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 4; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512
    }
    else if ( (strcmp(path+1, mis_datos->fichero_inicial) == 0) ) // fichero
    {
        stbuf->st_mode = S_IFREG | 0666;
        stbuf->st_nlink = 1; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = mis_datos->st_size; //tamaño
        stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512

    } else
        res = -ENOENT;

    return res;
}

static int ejemplo_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                           off_t offset, struct fuse_file_info *fi)
{
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;

    if (strcmp(path, "/") == 0)
    {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, mis_datos->fichero_inicial, NULL, 0)!=0) return -ENOMEM;
    }
    else {
        return -ENOMEM;
    }
    return 0;
}

static int ejemplo_open(const char *path, struct fuse_file_info *fi)
{
    struct structura_mis_datos* mis_datos = (struct structura_mis_datos*)fuse_get_context()->private_data;
    char lpath[1024];
    if (strcmp(path+1, mis_datos->fichero_inicial) == 0)
    {
        if ((fi->flags & 3) != O_RDONLY) return -EACCES;
        fi->fh=open(mis_datos->path,fi->flags);
        if(fi->fh<0) return -errno;
    }
    else
        return -ENOENT;

    return 0;
}

static int ejemplo_read(const char *path, char *buf, size_t size, off_t offset,
                        struct fuse_file_info *fi)
{
    return pread(fi->fh, buf, size, offset);
}

int ejemplo_release(const char *path, struct fuse_file_info *fi)
{
    close(fi->fh);
}

static struct fuse_operations ejemplo_oper = {
    .getattr	= ejemplo_getattr,
    .readdir	= ejemplo_readdir,
    .open		= ejemplo_open,
    .read		= ejemplo_read,
    .release	= ejemplo_release
};

int main(int argc, char *argv[])
{
    struct structura_mis_datos* mis_datos;
    mis_datos = malloc(sizeof(struct structura_mis_datos));
    FILE *f;
    struct stat fileStat;

    // análisis parámetros de entrada
    if ((argc < 3) || (argv[argc - 2][0] == '-') || (argv[argc - 1][0] == '-'))
    {
        perror("Parametros insuficientes");
        exit(-1);
    }

    mis_datos->fichero_inicial = strdup(argv[argc - 2]); // fichero original
    argv[argc - 2] = argv[argc - 1];
    argv[argc - 1] = NULL;
    argc--;

    // leer metadatos del fichero
    mis_datos->path = realpath(mis_datos->fichero_inicial, NULL);
    fprintf(stderr,"real path: %s\n",mis_datos->path);
    f=fopen(mis_datos->fichero_inicial,"rc");
    if (f==NULL) {
        perror ("error imposible abrir fichero");
        exit(-1);
    }
    fstat(fileno(f), &fileStat);
    mis_datos->st_uid= fileStat.st_uid;
    mis_datos->st_gid= fileStat.st_gid;
    mis_datos->st_atime = fileStat.st_atime;
    mis_datos->st_ctime = fileStat.st_ctime;
    mis_datos->st_mtime = fileStat.st_mtime;
    mis_datos->st_size = fileStat.st_size;
    fclose(f);
    return fuse_main(argc, argv, &ejemplo_oper, mis_datos);
}
