# ejemplo de uso FUSE para DSO 0
# hemos instalado el paquete de desarrollo de FUSE con:
# sudo apt-get install fuse-dev

fuse_flags= -D_FILE_OFFSET_BITS=64 -lfuse -pthread

.PHONY : mount umount test

ejemplo_fuse : ejemplo_fuse.c
	gcc  -o $@ $^ ${fuse_flags}
	mkdir -p punto_montaje
mount : 
	./ejemplo_fuse fichero.txt punto_montaje
debug :
	./ejemplo_fuse -d fichero.txt punto_montaje 
umount :
	fusermount -u punto_montaje
test : 
	ls -lah punto_montaje
	tail punto_montaje/fichero.txt

